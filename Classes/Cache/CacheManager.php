<?php
namespace KITT3N\Kitt3nServices\Cache;

/***
 *
 * This file is part of the "KITT3N | kitt3n services" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019
 *
 ***/

/**
 * The Cache Manager
 */
class CacheManager extends \TYPO3\CMS\Core\Cache\CacheManager
{

    /**
     * @param bool $disableCaching
     */
    public function __construct(bool $disableCaching = false)
    {
        parent::__construct($disableCaching);
    }

    /**
     * Override cacheConfigurations
     */
    public function overrideCacheConfigurations(array $aCacheConfigurations)
    {
        $this->cacheConfigurations = $aCacheConfigurations;
    }

    /**
     * Flushes all registered caches
     */
    public function flushCaches()
    {
        parent::flushCaches();
    }
}
